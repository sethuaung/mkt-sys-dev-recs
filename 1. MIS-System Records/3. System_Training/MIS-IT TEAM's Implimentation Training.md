[<img src="../../1. MIS-System Records/img/logo.png" width="50"/>](logo.png)

# MIS/IT TEAM's Implementation Training

`MIS-System Renovation` ပြီးဆုံးသည့်ကာလမှစ၍ (1 Nov 2022) မှ (30 Nov 2022) ထိ MIS/IT Team မှ သက်ဆိုင်ရာ ရုံးခွဲ `Township Accountant` များကို `System` နှင့်ပတ်သတ်၍ `End-User Training` ပေးခဲ့ပါသည်။ ထိုမှတစ်ဆင့် `Township Accountant` များသည် သက်ဆိုင်ရာ ရုံးခွဲ၏ ဝန်ထမ်းများအားလုံး `(BM, ABM, Accountant)` ကို (7 Dec 2022) မှ (31 Dec 2022) ထိ `End-User System Training` ပေးခဲ့ပါသည်။ MIS/IT Team မှ Training ပေးထားသော `Township Accountant` များကို `TOT Certificates` များ ပေးအပ်ခဲ့ပါသည်။ TOT Certificate [[Ath-15]]([Ath-15]_Branch_Training_Records.pdf) နှင့် `End-User Training` ပေးခဲ့သော မှတ်တမ်းများအား ပူးတွဲ၍ ဖော်ပြထားပါသည်။

<details><summary>Township Accountant Assign [Ath-16]</summary><img src="[Ath-16]_TA_Assign_Branch_Training.png" name="[Ath-16]"></details>

<details><summary>End-user Training Record [Ath-17]</summary><img src="[Ath-17]_Branch_Training.png"></details>