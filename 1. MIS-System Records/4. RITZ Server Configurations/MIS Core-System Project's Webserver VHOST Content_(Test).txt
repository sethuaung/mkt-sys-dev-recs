++++++++++++++++++++++++++++++++++++++++++++++++++++++++
NGINX Webserver VHOST Content
++++++++++++++++++++++++++++++++++++++++++++++++++++++++


URL	:	https://rent.as137431.net/




mkt@mis:~$ cat /etc/nginx/sites-available/core-upgrade

```
Output:
```

## MIS-System Configuration Content ##
    server {
    listen 443 ssl;
    listen [::]:443 ssl;
    server_name rent.as137431.net;

    root /var/www/html/mis-system-upgrade/public;
    index index.php;

    ssl_certificate /etc/ssl/mkt_ssl/MKTcert3.pem;
    ssl_certificate_key /etc/ssl/mkt_ssl/MKTkey3.pem;

    access_log /var/log/nginx/nginx.access.log;
    error_log /var/log/nginx/nginx.error.log;

    location ~ .php$ {
    include snippets/fastcgi-php.conf;
    fastcgi_pass unix:/run/php/php7.4-fpm.sock;
    }

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }


    location /phpmyadmin {
    root /var/www/html/;
    index index.php;
    try_files $uri $uri/ =404;

    location ~* ^.+.(jpg|jpeg|gif|css|png|js|ico|xml)$ {
    access_log off; expires 30d;
    }

    location ~ /\.ht {
    deny all;
    }

    location ~ /(libraries|setup/frames|setup/libs) {
    deny all; return 404;
    }

    location ~ \.php$ {
    include snippets/fastcgi-php.conf;
    fastcgi_pass unix:/run/php/php7.4-fpm.sock;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}
}