[<img src="../1. MIS-System Records/img/logo.png" width="50"/>](logo.png)

# MIS Core-System

## Condition - (A)
MIS System (CloudNet Software) သည် [Mr.Kiry (CloudNet)](https://cloudnet.com.kh) မှ (Dec 2021) နေ့တွင် MKT ဘက်သို့ [Gitlab Project](https://gitlab.com/tonim962/microfinance-2019) ထဲတွင် ထည့်သွင်း၍ လွှဲပြောင်းပေးခဲ့ပါသည်။ Gitlab project ထဲသို့ လွှဲပြောင်းပေးခဲ့သည့် Project အခြေအနေမှာ ပူးတွဲပါ ဖိုင်အတိုင်း [[Ath-1]](/2. Doc/[Ath-1] _Cloudnet_Module_Dec_2021.pdf) ဖြစ်ပါသည်။
 
CloudNet Project Progress [[Ath-1]](/2. Doc/[Ath-1] _Cloudnet_Module_Dec_2021.pdf)

<details><summary>Project Source Code [Ath-2]</summary><img src="img/[Ath-2] _Cloudnet_handover_Web capture_17-2-2023_11142_gitlab.com.jpeg" name="[Ath-2]"></details>

```
Project Manager		:	Mya Thazin Han (Ritz)
Developer		:	CloudNet (Combodia)
Reporter		:	Myint Myat Moe (MKT)
Project Duration	:	2019 to Dec 2021
```

## Condition - (B)

ထိုမှတစ်ဆင့် MKT ဘက်မှ RITZ ဘက်သို့ (20 January 2022) နေ့တွင် Mail ပို့၍ [[Ath-3]](/2. Docs/[Ath-3]_SourceCode_Delivered_to_Ritz_20_January_2022.png) တစ်ဆင့် လွှဲပြောင်းပေးခဲ့ပါသည်။ RITZ သည် Source-Code လက်ခံရရှိသည့် (20 January 2022) နေ့မှ (22 August 2022) နေ့ထိ ထို Project အားပြင်ဆင်ခဲ့ပါသည်။ သို့ရာတွင် ဆက်လက် ပြင်ဆင်နိုင်ခြင်း မရှိတော့သည့်အတွက် (22 August 2022) နေ့တွင် မပြင်ဆင်နိုင်ကြောင်းကို MKT ဘက်သို့ Mail ပို့ [[Ath-4]](/2. Docs/[Ath-4]_RITZ_Reply_Aug_2022.jpg) အကြောင်းကြားခဲ့ပါသည်။ Ritz ဘက်မှ ပြင်ဆင်ခဲ့သည် Project အခြေအနေမှာ ပူးတွဲ [[Ath-5]](/2. Docs/[Ath-5]_MIS System_Project_Report_July_2022(To MD).pdf)ပါ အတိုင်းဖြစ်ပါသည်။ 

<details><summary>Ritz Project Progress[Ath-5]</summary><img src="2. Docs/[Ath-5]_MIS System_Project_Report_July_2022(To MD).pdf" name="[Ath-5]"></details>

<details><summary>Project Source Code [Ath-6]</summary><img src="img/[Ath-6]_RITZ_Web capture_14-2-2023_15846_gitlab.com.jpeg" name="[Ath-6]"></details>

```
Project Manager		:	Mya Thazin Han (Ritz)
Developer		:	Zay Lwin Naing (Ritz)
Reporter		:	Myint Myat Moe (MKT), Se Thu Aung (MKT)
Project Duration	:	20 January 2022 to 22 August 2022
```

## Condition - (C)

```RITZ``` ဘက်မှ Handover ပြုလုပ်ခဲ့သည့် Project မှာ ဆက်လက်ပြင်ဆင်ရန် အဆင်မပြေတော့သည့်အတွက် MKT ဘက်မှ ```Freelance Developer (Nyi Nyi)``` အား ဆက်လက်ပြင်ဆင်ရန် (27 August 2022)နေ့တွင် တာဝန်ပေးဆောင်ရွက်ခဲ့ပါသည်။ [Nyi Nyi](https://gitlab.com/asnyinyi26) ပြန်ပြင်သည့် Source-code မှာ CloudNet ဘက်မှ Handover ပြုလုပ်ထားသည့် [Source code](https://gitlab.com/tonim962/microfinance-2019) ပေါ်တွင်သာ  (27 August 2022) နေ့မှ (15 October 2022)နေ့ထိ ပြုပြင်ခဲ့ပါသည်။ 

<details><summary>Project Sign-Off [Ath-7]</summary><img src="2. Docs/[Ath-7]_MIS System Renovation Project Sign-off.pdf" name="[Ath-7]"></details>

<details><summary>Project Source Code [Ath-8]</summary><img src="img/[Ath-8]_MKT_Core_Renno_Oct_2022_Web capture_17-2-2023_111450_gitlab.com_.jpeg" name="[Ath-8]"></details>

```
Project Manager			:	Aye Myat Mon (Chin Corp)
Developer			:	Nyi Nyi (Freelance Developer)
Reporter			:	Myint Myat Moe (MKT), Se Thu Aung (MKT)
Project Duration		:	27 August 2022 to 15 October 2022
Digital License Maker	        :	Se Thu Aung (MKT)
```

ထိုပြင်ခဲ့သည့်ကာလအတွင်း System အခြေအနေမှာ အောက်ပါအတိုင်းဖြစ်ပါသည်။

System တွင် Main Module (9) ခု ပါဝင်ပါသည်။

- 1. Client Module
- 2. Loan Module
- 3. Payment Module
- 4. Saving Module (Compulsory Saving)
- 5. Loan Transfer Module
- 6. Product Module
- 7. Accounting Module
- 8. Report Module (Operation Report & Accounting Report)
- 9. General Setting (Permission, other setting)

အထက်ပါ Module များကို ```CloudNet``` ဘက်မှ Handover ပြုလုပ်ခဲ့စဉ်က ကျန်ရှိနေသည့် ``Error`` များကို ဖြေရှင်းပေးခဲ့ပါသည်။ သို့ရာတွင် `Accounting Module` သည် `Finance Department` ဘက်မှ စစ်ဆေးရန်ကျန်ရှိနေသည့်အတွက် လိုအပ်ချက်များ ရှိနေပါသည်။

### Final Condition :
ထိုမှတစ်ဆင့် ``MKT's MIS/IT Team`` မှ ရည်ရှည်တွင် **MIS-System** ပိုမို အဆင်ပြေချောမွေ့နိုင်စေရန်အတွက် (7th January 2023) နေ့မှ (9th January 2023) နေ့ အတွင်း ``Developer`` အား မိမိတို့ စရိတ်ဖြင့် [Vou[Ath-9]](2. Docs/[Ath-9]_Vou_Jan_2023_Dev.png) `Accounting Report` များနှင့် အခြားသက်ဆိုင်ရာ လိုအပ်ချက်များ ပြင်ဆင်ရေးကို ဆောင်ရွက်ခဲ့ပါသည်။ 

ပြင်ဆင် မွမ်းမံခဲ့သည့် အချက်အလက်များမှာ အောက်ပါအတိုင်းဖြစ်ပါသည်။

- 1. Added Client's Old ID Filed in Client & Loan
- 2. Fixed to view & print deposit & disburse voucher for each client
- 3. Fixed to show branch name & client ID in deposit & disburse voucher
- 4. When delete loan, it deleted all transactions with relative loan
- 5. Added Sub-type & Parent with C.O.A
- 6. Fixed to show all C.O.A Account Code in Trial report
- 7. Fixed P & L Report with relative C.O.A Code
- 8. Fixed Balance Sheet Report with relative C.O.A Code

**Remark :** 

System ထဲရှိ `Accounting Report` များသည် ယခင်က `Finance Department` မှ ပေးထားသော `C.O.A` အတိုင်း Report များတွင် `C.O.A Code` များအား *excel-format* အတိုင်း (Static) အသေပုံစံဖြင့် ဖော်ပြထားခြင်းဖြစ်သည်။ ယခင်က `C.O.A Code` များကို အသေရေးထားသည့်အတွက် (update) အပြောင်းအလဲပြုလုပ်လိုလျှင်ဆင်မပြေနိုင်သောကြောင့် `Dynamic` ပုံစံပြောင်းလဲထားပါသည်။ ယခုအခါတွင် `COA Code` များကို သက်ဆိုင်ရာ `Parent & Sub-Type` များ ချိတ်ဆက်မှု မှန်ကန်ပါက `C.O.A Code` အသစ်ထပ်တိုးသည့်အခါတွင်လည်း သက်ဆိုင်ရာ `C.O.A Code` များ အောက်တွင် ဒေတာများ ဝင်ရောက်မည်ဖြစ်သည်။

ထိုသို့ ပြင်ဆင်မှု နောက်ပိုင်းတွင် `System UI/UX Design` ပိုင်းနှင့် အခြားသော `Minor-Cases` များကို `MIS/IT Team` မှ ဆက်လက်ပြင်ဆင်ခဲ့ပါသည်။ ထို့ကြောင့် လက်ရှိ System ၌ `Operation` နှင့်ပတ်သတ်သည့် အပိုင်းအားလုံးတွင်အချက်အလက်များ System ထဲသို့ မှန်ကန်စွာ ဖြည့်သွင်းပါက မှန်ကန်စွာ အသုံးပြုနိုင်သည် ဖြစ်ပါသည်။ `Accounting Reports` နှင့် ပတ်သတ်၍ `Finance Department` မှ `တိကျမှန်ကန်` သည့် `ဒေတာများ` ဖြည့်စွက်၍ `အစုံအလင် စမ်းသပ် စစ်ဆေး` အသုံးပြုထားခြင်း`မရှိသည့်`အတွက် လိုအပ်ချက်များရှိနိုင်ပါသည်။

```
Project Handler				:	Se Thu Aung
Developer				:	Se Thu Aung (MKT), Myint Myat Moe (MKT)
Reporter				:	Aung Thu (MKT), Hein Thant Zin (MKT), Aye Nandar Htun (MKT)
Project Duration			:	Nov 2022 to Feb 2023
Digital License Maker		        :	Se Thu Aung (MKT)
```

<details><summary>**Final** Project Source Code [Ath-10]</summary><img src="img/[Ath-10]_ MIS_CoreWeb capture_14-2-2023_15935_gitlab.com_data.jpeg" name="[Ath-8]"></details>

