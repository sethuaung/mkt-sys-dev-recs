[<img src="../1. MIS-System Records/img/logo.png" width="50"/>](logo.png)

# ME-Loan Survey Application

ME-Loan Survey Web & Mobile App သည် SMD Company နှင့် မြတ်ကျွန်းသာတို့ ပူးပေါင်း၍ ပြုလုပ်ထားခြင်းဖြစ်သည်။ Project တစ်ခုလုံး၏ Source Code သည် မြတ်ကျွန်းသာ၏ မူပိုင်ဖြစ်သည်။ ထို့ကြောင့် မည်သူတစ်ဦးတစ်ယောက်မျှ မြတ်ကျွန်းသာ၏ ခွင့်ပြုချက်မရှိဘဲ ကူးယူဖြန့်ဝေသုံးစွဲခွင့်မရှိပါ။ အဆိုပါ Source Code ကို Version Control ဖြစ်သော Gitlab ပေါ်တွင် MIS/IT Team မှ Company ပိုင် Gitlab account ပေါ်တွင် သိမ်းဆည်းထားပါသည်။ 

<details><summary>Project Proposal [Ath-11]</summary><img src="1. ME Loan Contract & Proposal/[Ath-11]_Me Loan_SMD Proposal.pdf" name="[Ath-11]"></details>

<details><summary>Source Code Contract [Ath-12]</summary><img src="1. ME Loan Contract & Proposal/[Ath-12]_ME Loan_Source Code Contract.pdf" name="[Ath-12]"></details>

* Userguide location  ```/2. MKT UserGuide/ME_Survey Mobile App User Guide.pdf and ME_Survey Web App User Guide.pdf```

<details><summary>Project Source Code [Ath-13]</summary><img src="img/[Ath-13]_ME_Loan_Web capture_14-2-2023_1582_gitlab.com_data.jpeg" name="[Ath-13]"></details>

```
Developer			:	Kyi Htwe Han (SMD)

Project Duration	:	August 2020 to 20 January 2021
	
Importer			:	Se Thu Aung(MKT), Myint Myat Moe

Digital License		:	Se Thu Aung (MKT)

```
