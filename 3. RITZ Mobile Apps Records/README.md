[<img src="../1. MIS-System Records/img/logo.png" width="50"/>](logo.png)

# RITZ's Mobile Apps (Peripheral of MIS Core-System)
RITZ's Mobile App သည် (Dec 2021) မှ (Nov 2022) ထိ သတ်မှတ်ထားပြီး ယခု (Feb 2023) ထိ Developing process မပြီးသေးပါ။ အဆင့်အဆင့် ပြုလုပ်ခဲ့သော Contract များအရ (Nov 2022) တွင် ပြီးဆုံးရမည်ဖြစ်သောကြောင့် RITZ ဘက်မှ Delivered ပို့ပေးသော Application အား MKT's MIS Team မှ စစ်ဆေးရာတွင် MKT ၏ Methodology နှင့် Core-System ၏ Algorithms များနှင့် မကိုက်ညီဘဲ လိုအပ်ချက်များစွာ ရှိနေသည့် အခြေအနေအဖြစ် ရှိနေပါသည်။


ထို့ကြောင့် Ritz Developer Team မှ (Jan 2023) တွင် MKT Mandalay ရုံးချုပ်သို့လာရန် MKT MIS Team နှင့် အတူ လိုအပ်သည့်အချက်အလက်များအား ဆွေးနွေးတိုင်ပင်ခဲ့ပါသည်။ ထို့နောက် Project Dead-line အား (၂) ပတ်အတွင်း အပြီးသတ်ပေးမည်ဟု Development Team ၏ Leader ဖြစ်သော Zay Lwin Naing (RITZ) မှ MKT's M.D သို့ ကတိပေးပြောဆိုခဲ့ပါသည်။


```
Project Coordinator		:	Aye Myat Mon (Chin Corp)

Developer			:	Zay Lwin Naing, Kaung Htaik San, Kaung Min Htet (Ritz)

Project Duration		:	Dec 2021 to Feb 2023

Project Importer		:	Se Thu Aung (MKT)

Issue Reporter			:	Myint Myat Moe (MKT), Aung Thu (MKT),
					Hein Than Zin (MKT), Aye Nandar Htun (MKT)
```

<details><summary>January 2023 Mobile Application Report [Ath-14]</summary><src="[Ath-14]_RITZ_Mobile_Test-Result-Evaluation-Report_Jan_2023.pdf" name="[Ath-14]"></details>

<details><summary>Demo Source Code</summary><[Web](https://gitlab.com/sethuaung/ritz-web-portal), [Mobile](https://gitlab.com/sethuaung/mkt_api_mobile)"></details>

