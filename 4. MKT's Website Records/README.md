[<img src="../1. MIS-System Records/img/logo.png" width="50"/>](logo.png)

# MKT Website Records
[MKT Website](http://mkt.com.mm/) သည် ယခင် UI Design ကို [Se Thu Aung](https://gitlab.com/sethuaung) (MKT) မှ ပြန်လည်ပြင်ဆင်ခဲ့ပါသည်။

- Project URL	:	http://mkt.com.mm/

- Developer	:	[Se Thu Aung](https://gitlab.com/sethuaung) (MKT)

- Website-Backup Export File Location
```
/4. MKT's Website Records/website_export/###.xml
```

###Summary:

<details><summary>Home page</summary><img src="img/home_1.jpeg" name="Home page"></details>

<details><summary>Product and Services</summary><img src="img/pns_1.jpeg" name="Product and Services"></details>

<details><summary>Plugins</summary><img src="img/plugins.jpeg" name="Plugins"></details>

<details><summary>Users</summary><img src="img/admin_users.jpeg" name="Users"></details>
