[<img src="1. MIS-System Records/img/logo.png" width="50"/>](logo.png)

# Handover Documentations

**Time: February 2023**

**From:**
> [Se Thu Aung](https://gitlab.com/sethuaung) (Assistant Manager) | [Myint Myat Moe](https://gitlab.com/mudrakhin) (Officer)
>	
> MIS/IT Department


**To:**
> [Su Sandar Myint](mailto:yanyansu@mkt.com.mm) (Managing Director)
> *[Myat Kyun Thar Microfinance Limited](http://mkt.com.mm/)*


**Location:**
> [Myat Kyun Thar Microfinance](http://mkt.com.mm/) | 4th floor | [Chin Corp Myanmar Office Tower](https://goo.gl/maps/4dpDaxmfAmq8NpRz6) | Mandalay | Myanmar.


## Description

This is the handover documentation page for Myat Kyun Thar Microfinance's MFI Software Project. Compiled and arranged by [Se Thu Aung](https://gitlab.com/sethuaung).

The document contain followings;

> 1. [MIS Core System Documentation](1. MIS-System Records) (Project Starting to Handover Period, End-User Training, RITZ Server Configurations)
>
> 2. [ME-Loan Survey Documentation](2. ME-Loans Survey App Records)
>
> 3. [RITZ's Mobile Apps Documents](3. RITZ Mobile Apps Records)
>
> 4. [MKT Website](4. MKT's Website Records) (Initial Rebuild export files)

## Authors and acknowledgment

> MIS System's Blueprint and Digital License are created by [Se Thu Aung](https://gitlab.com/sethuaung) and some document of MIS Core System is own creation by himself.
> MIS Core System and ME Loan Survey System are definitly owned by MKT. So, anyother are cann't copy, edit and other relative process.

## License

For open source projects license and MKT's Digital License.

## Project Participants

From **May 2018** to **Feb 2023**, [Myint Myat Moe](https://gitlab.com/mudrakhin) and MIS-Team Members were trying to stablized the system. Nowaday, the MIS Core-System is 99% Completed and ME-Loan Survey Application is ready to use.
